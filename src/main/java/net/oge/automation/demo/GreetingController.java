package net.oge.automation.demo;

import java.net.URI;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

	private static final String TEMPLATE = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		String value = null;
		if(name.length() < 100) {
			value = name;
		}
		return new Greeting(counter.incrementAndGet(), this.getGreetingText(value));
	}

    @GetMapping("/open-redirect")
    public ResponseEntity<Void> redirect(@RequestParam(value = "url", required = true) String url) {
		final URI redirectUri = URI.create(url);
        return ResponseEntity.status(HttpStatus.FOUND).location(redirectUri).build();
    }
	
	public ResponseEntity<String> compare(@Nullable @RequestParam(value = "firstName", required = false) String firstName, @RequestParam(value = "lastName") String lastName) {
		if (firstName.equals(lastName)) {
			return ResponseEntity.badRequest().build(); 
		}
		return ResponseEntity.ok().build();
	}

	private String getGreetingText(@NonNull String name) {
		return String.format(TEMPLATE, name);
	}
}
